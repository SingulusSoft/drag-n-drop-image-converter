﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMain))
        Me.brwseBtn = New System.Windows.Forms.Button()
        Me.pathBox = New System.Windows.Forms.TextBox()
        Me.extComBox = New System.Windows.Forms.ComboBox()
        Me.SuspendLayout()
        '
        'brwseBtn
        '
        Me.brwseBtn.Location = New System.Drawing.Point(197, 12)
        Me.brwseBtn.Name = "brwseBtn"
        Me.brwseBtn.Size = New System.Drawing.Size(75, 23)
        Me.brwseBtn.TabIndex = 0
        Me.brwseBtn.Text = "Browse"
        Me.brwseBtn.UseVisualStyleBackColor = True
        '
        'pathBox
        '
        Me.pathBox.Location = New System.Drawing.Point(12, 15)
        Me.pathBox.Name = "pathBox"
        Me.pathBox.ReadOnly = True
        Me.pathBox.Size = New System.Drawing.Size(179, 20)
        Me.pathBox.TabIndex = 1
        '
        'extComBox
        '
        Me.extComBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.extComBox.FormattingEnabled = True
        Me.extComBox.Items.AddRange(New Object() {"jpg", "png", "gif", "bmp"})
        Me.extComBox.Location = New System.Drawing.Point(12, 68)
        Me.extComBox.Name = "extComBox"
        Me.extComBox.Size = New System.Drawing.Size(121, 21)
        Me.extComBox.TabIndex = 2
        '
        'frmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(284, 119)
        Me.Controls.Add(Me.extComBox)
        Me.Controls.Add(Me.pathBox)
        Me.Controls.Add(Me.brwseBtn)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmMain"
        Me.Text = "Converter Settings"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents brwseBtn As System.Windows.Forms.Button
    Friend WithEvents pathBox As System.Windows.Forms.TextBox
    Friend WithEvents extComBox As System.Windows.Forms.ComboBox

End Class

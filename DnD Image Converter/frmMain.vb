﻿Public Class frmMain

    Private Sub brwseBtn_Click(sender As Object, e As EventArgs) Handles brwseBtn.Click
        Dim fldr As New FolderBrowserDialog
        If fldr.ShowDialog = Windows.Forms.DialogResult.OK Then
            My.Settings.SavPath = fldr.SelectedPath
            My.Settings.Save()
            pathBox.Text = fldr.SelectedPath
        End If
    End Sub

    Private Sub frmMain_Load(sender As Object, e As EventArgs) Handles Me.Load
        If My.Settings.SavPath = Nothing Then
            My.Settings.SavPath = Environment.GetFolderPath(Environment.SpecialFolder.MyPictures)
        End If
        pathBox.Text = My.Settings.SavPath
        extComBox.SelectedItem = My.Settings.SavExt
        If My.Application.CommandLineArgs.Count = 0 Then
        Else
            If Not IO.Directory.Exists(My.Settings.SavPath) Then
                IO.Directory.CreateDirectory(My.Settings.SavPath)
            End If
            If My.Settings.SavExt = "jpg" Then
                For pic As Integer = 0 To My.Application.CommandLineArgs.Count - 1
                    Dim newimage As Image = Image.FromFile(My.Application.CommandLineArgs(pic))
                    newimage.Save(My.Settings.SavPath & "/" & IO.Path.GetFileNameWithoutExtension(My.Application.CommandLineArgs(pic)) & ".jpg", Imaging.ImageFormat.Jpeg)
                Next
            End If
            If My.Settings.SavExt = "png" Then
                For pic As Integer = 0 To My.Application.CommandLineArgs.Count - 1
                    Dim newimage As Image = Image.FromFile(My.Application.CommandLineArgs(pic))
                    newimage.Save(My.Settings.SavPath & "/" & IO.Path.GetFileNameWithoutExtension(My.Application.CommandLineArgs(pic)) & ".png", Imaging.ImageFormat.Png)
                Next
            End If
            If My.Settings.SavExt = "gif" Then
                For pic As Integer = 0 To My.Application.CommandLineArgs.Count - 1
                    Dim newimage As Image = Image.FromFile(My.Application.CommandLineArgs(pic))
                    newimage.Save(My.Settings.SavPath & "/" & IO.Path.GetFileNameWithoutExtension(My.Application.CommandLineArgs(pic)) & ".gif", Imaging.ImageFormat.Gif)
                Next
            End If
            If My.Settings.SavExt = "bmp" Then
                For pic As Integer = 0 To My.Application.CommandLineArgs.Count - 1
                    Dim newimage As Image = Image.FromFile(My.Application.CommandLineArgs(pic))
                    newimage.Save(My.Settings.SavPath & "/" & IO.Path.GetFileNameWithoutExtension(My.Application.CommandLineArgs(pic)) & ".bmp", Imaging.ImageFormat.Bmp)
                Next
            End If
            Me.Close()
        End If
    End Sub

    Private Sub extComBox_SelectedValueChanged(sender As Object, e As EventArgs) Handles extComBox.SelectedValueChanged
        My.Settings.SavExt = extComBox.SelectedItem
    End Sub
End Class

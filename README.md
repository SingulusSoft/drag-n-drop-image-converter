# README #

This README is for Drag and Drop Image Converter by SingulusSoft.

### What is this repository for? ###

* DnD Image Converter is a quick and easy way to convert images.
* 1.0

### How do I get set up? ###

* Download the source and open the project file.
* DnD was made using Visual Studio Ultimate 2013 and .Net Framework 4.5

### How To Use Drag N Drop Image Converter ###

* To change settings such as file type to convert to and location to save converted images, run the program regularly. 

* To perform a conversion, select all the images you want to convert, drag, then drop them onto the programs executable. 

### Contribution guidelines ###

* Feel free to contribute how you desire.

### Who do I talk to? ###

* Repo owner or admin


